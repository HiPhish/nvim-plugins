These files are various Git hooks which specify extra steps for the plugins.
Symlink them into the respective hooks directory of each package. Hooks usually
contain extra steps to take after a plugin has been updated, such as compiling
code.

.. code:: sh

   # Called from within the hooks directory
   for d in *; do
      for f in "$d/*"; do
         ln -s "$d/$f" "../.git/modules/start/$d/hooks/$f"
      done
   done
