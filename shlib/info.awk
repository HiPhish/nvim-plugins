# Parses the .gitmodules file and prints the name of modules

/\[submodule .*\]/ {
	match($0, /\[submodule "(start|opt)\/(.*)"\]/, info)
	module["kind"] = info[1]
	module["name"] = info[2]
	print module["name"]
}

/	path = .*/ {
	module["path"] = $3
}

/	url = .*/ {
	module["url"] = $3
}
