This file contains plugin-specific settings. Only the bare minimum should be
stored here, the rest goes into the actual Neovim configuration.
