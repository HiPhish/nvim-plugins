.. default-role:: code


##########################
 My custom Neovim plugins
##########################

This repository contains my personal Neovim package collection. Packages are
specified as Git submodules.


Installation
############

Clone this repository into a directory in your package path, initialise
submodules. Example:

.. code:: sh

   # Important: Intialise submodules
   git clone --recursive <repo> ~/.local/share/nvim/site/pack/custom-plugins


Synchronisation
###############

After pulling changes to this repository the submodules will probably still be
in the wrong state. We have to synchronise them to match the `.gitmodules`
file.

.. code:: sh

   git submodule update --checkout


Handy snippets
##############

Because I can't be bothered to look up the Git manual every time.


Update all plugins
==================

.. code:: sh

   git submodule update --remote



Update a single plugin
======================

.. code:: sh

   git submodule update --remote start/some-plugin

Remove a submodule
==================

.. code:: sh

   git rm start/cool-plugin.nvim


Afterwards commit the changes. Some files are kept around as cache in case the
submodule is checked out again. To scrub away *all* traces:

.. code:: sh

   rm -rf .git/modules/start/cool-plugin.nvim
   git config --remove-section submodule.start/cool-plugin.nvim
